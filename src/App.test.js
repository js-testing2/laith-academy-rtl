import { render, screen } from "@testing-library/react";
import App from "./App";

// test("renders learn react link", () => {
//   // 1) Rendering component we want to test
//   render(<App />); // passing in the component - renders component and any children into the virtual dom

//   // 2) Finding a specific element inside the component, regular expression to find the text that matches, gets us the anchor tag
//   const linkElement = screen.getByText(/learn react/i);

//   // 3) Assertion - Final thing that asserts what we want in the test. Whats going to fail or pass
//   expect(linkElement).toBeInTheDocument();
// });

// // describe what the test is testing
// test("description", () => {});

// queryBy and getBy are the same but different when there are 0 matches
// getBy - throws an error with 0 matches, returns 1 match, throws error if it find multiple matches, cant be used asynchronously
// queryBy - returns null if 0 matches are found - useful for determining if you want to test for a lack of a UI element in a component, cant be used asynchronously
// findBy - behaves same as getBy but can be used asynchronously for async elements
// always look in the docs for the order of getting an element such as getByRole, getByAltText as the first ones are better for screen readers and semantics. Getbytesid should be last resort.
test("inputs should be initially empty", () => {
  // render component
  render(<App />);
  // queries - choose the correct queries to get the elements we need
  // we can retrieve the username field by getByRole, however the password field has no implicit role, so we use getByLabelText
  // each label is attached to the input so we can get the input via the label do we use htmlFor and id
  // const emailInputElement = screen.getByRole("textbox");
  const emailInputElement = screen.getByRole("textbox");
  // const passwordInputElement = screen.getByLabelText("Password");
  const passwordInputElement = screen.getByLabelText(/password/i);
  const confirmPasswordInputElement =
    screen.getByLabelText(/confirm password/i);

  expect(emailInputElement.value).toBe("");
  expect(passwordInputElement.value).toBe("");
  expect(confirmPasswordInputElement.value).toBe("");
});
